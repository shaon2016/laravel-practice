@extends('layout')

@section('body')
    <h1>All Cards</h1>

    @foreach($allCards as $card)

        <div>
            <a href="/cards/{{$card->id}}">{{$card->title}}</a>
        </div>
    @endforeach
@stop