<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use App\Card;
use App\Http\Requests;

class NotesController extends Controller
{
    public function store(Card $card) {
        //return $card;
        $note = new Note;
        $note->noteBody = request()->noteBody;
        $card->notes()->save($note);

        return back();
    }
    
    public function edit(Note $note) {
        return view('notes.edit', compact('note'));
    }
    
    public function update(Request $request, Note $note) {
        $note->update($request->all());
        return back();
    }
}
