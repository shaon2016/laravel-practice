<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Http\Request;

use App\Http\Requests;

class CardsController extends Controller
{
    public function index()
    {
        //$allCards = \DB::table('cards')->get();
        // eloquent
        $allCards = Card::all();
        return view('cards.index', compact('allCards'));
    }
    
    public function show(Card $card) {
        //$card = Card::find($id);
        return view('cards.show', compact('card'));
    }
}
