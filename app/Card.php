<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Card extends Model
{
    public function notes () {
        return $this->hasMany(Note::class);
    }
}
