@extends('layout')

@section('body')
    <h1>Edit this note</h1>
    <form method="POST" action="/notes/{{$note->id}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        {{method_field('patch')}}

        <div class="form-group">
            <textarea name="noteBody"
                      class="form-control">{{$note->noteBody}}</textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update Note</button>
        </div>
    </form>
@stop