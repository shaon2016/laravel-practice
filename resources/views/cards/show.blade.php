@extends('layout')

@section('body')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>{{$card->title}}</h1>

            <ul class="list-group">
                @foreach($card->notes as $allNotesForASingleCard)
                    <li class="list-group-item">{{$allNotesForASingleCard->noteBody}}
                        <a href="../notes/{{$allNotesForASingleCard->id}}/edit" style="float: right">Edit</a>
                    </li>
                @endforeach
            </ul>

            <h3>Add Note</h3>

            <form method="POST" action="/cards/{{$card->id}}/notes">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <textarea name="noteBody" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Add Note</button>
                </div>
            </form>
        </div>
    </div>
@stop